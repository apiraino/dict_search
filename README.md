### An extension for Thunderbird (60+) to lookup words on a online dictionary

This Thunderbird extension relies on the old, deprecated-but-not-yet-replaced [XUL API](https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XUL). Thunderbird, too, will benefit at some point of the WebExtensions new API for the add-ons. But that day has yet to come and [I'm not holding my breath](https://wiki.mozilla.org/Thunderbird/Add-ons_Guide_57).

Meanwhile, one can still write an extension! Documentation, HOW-TOs are still available starting from the [XUL wiki](https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Thunderbird_extensions), a small tutorial can be found [here](https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Thunderbird_extensions/Building_a_Thunderbird_extension); these pages, however, are now abandoned therefore you'll crash into many 404s. To fill in the blanks, you'll end up mostly - like I did - downloading other add-ons and learn by copy and paste.

In my case a lot of credit is due to the [URLlink add-on](https://github.com/fnxweb/urllink/blob/master/urllink-3.3.2.xpi) that basically I used as a guide dog.

### Packing the extension for installation

Move to the root source code directory:
``` bash
$ tree
.
├── chrome.manifest
├── content
│   ├── dict_search.xul
│   ├── flags32.png
│   ├── flags64.png
│   ├── flags96.png
│   └── search.js
├── install.rdf
└── README.md

2 directories, 11 files
```

Then, execute:
``` bash
$ rm ~/tmp/dict_search_0.1.xpi
$ zip -vrX9 -xREADME.md ~/tmp/dict_search_0.1.xpi *

```

The `.xpi` file can then be manually loaded in Thunderbird

### Publish on Mozilla Add-ons site

- Go to https://addons.thunderbird.net/thunderbird
- log in
- upload the `.xpi` file

### Notes

This Thunderbird extension solves just one very specific case use for me (querying `dict.leo.org`), I took it more as an
exercise to learn a little bit about XUL extensions. I'm not really motivated enough to extend it beyond this point,
but - hey - pull requests are always welcome ;-)
